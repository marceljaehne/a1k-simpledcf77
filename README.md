# A1k SimpleDCF77

This is a DCF77 module adapter for the serial port of the Amiga. It should work with all DCF77 modules between 1.5v-3.3v and 5V.  
Build of docs from http://aminet.net/package/docs/hard/DCF77_Plus1.22.  
Thx to Peter Reibold!  
  
https://mytube.madzel.de/w/1ZwWwtCRpL6a2XFFqnmcv5 
  
## Ever wanted the right time on your Amiga? Get this module! :-)
  
![A1kSimpleClock](22-02-19_11-55-21_0884.jpg)  
---    

**DISCLAIMER!!!!  
**THIS IS A HOBBYIST PROJECT! I'M NOT RESPONSIBLE IF IT DOESN'T WORK OR DESTROYS YOUR VALUABLE DATA OR HARDWARE!  





